var Dashboard = function() {
	"use strict";
	var currentChallenges = function() {
		$.ajax({
			type: 'POST',
			url: 'http://localhost/test.php',
			data: { 'type': 'ongoing' },
			success: function(response,status) {
				if(status=200) {
					console.log(response);
				} else {
					Materialize.toast('There was some error while retrieving the data', 4000);
				}
			},
			error: function(response) { Materialize.toast('There was some error while retrieving the data', 4000); }
		});
	},
	monthly_data = function() {
		
	};

	return {
		init : function() {
			currentChallenges();
			monthly_data();
		}
	};
}();