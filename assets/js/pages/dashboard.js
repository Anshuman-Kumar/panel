var Dashboard = function() {
    "use strict";
    var currentChallenges = function() {
        $.ajax({
            type: 'POST',
            url: 'http://localhost/test.php',
            data: { 'type': 'ongoing' },
            success: function(response,status) {
                if(status=200) {
                    var trHTML = '', data = $.parseJSON(response);      
                    $.each(data, function (i, item) {
                        
                        trHTML += '<tr><td>' + data[i].name + '</td><td>' + data[i].title + '</td><td>' + data[i].remaining + 'hr</td><td>' + data[i].point + 'pts</td> </tr>';
                    });
                    $('#ongoing').append(trHTML);
                } else {
                    Materialize.toast('There was some error while retrieving the data', 4000);
                }
            },
            error: function(response) { Materialize.toast('There was some error while retrieving the data', 4000); }
        });
    },
    monthly_data = function() {
        $.ajax({
            type: 'POST',
            url: 'http://localhost/test.php',
            data: { 'type': 'monthly' },
            success: function(response,status) {
                if(status=200) {
                    var trHTML = '', data = $.parseJSON(response);      
                    $.each(data, function (i, item) {
                        
                        trHTML += '<tr><td>' + data[i].user_id + '</td><td>' + data[i].name + '</td><td>' + data[i].total_challenges + '</td><td>' + data[i].points + 'pts</td></tr>';
                    });
                    $('#monthly').append(trHTML);
                } else {
                    Materialize.toast('There was some error while retrieving the data', 4000);
                }
            },
            error: function(response) { Materialize.toast('There was some error while retrieving the data', 4000); }
        });
    },
    get_profile = function() {
        var userdata = localStorage.getItem("userdata");
        if(userdata){
            userdata = $.parseJSON(userdata);
            $('.user_name').html(userdata.name.toUpperCase());
            $('.user_email').html(userdata.email+'<i class="material-icons right">arrow_drop_down</i>');
        }else{
            window.location.href = 'login.html';
        }
    };

    return {
        init : function() {
            currentChallenges();
            monthly_data();
            get_profile();
        }
    };
}();