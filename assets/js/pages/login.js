var Login = function() {
    "use strict";
    var url = "http://localhost/html/demo.php";
    var user_register = function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: url,
            data: $("#register").serialize(),
            success: function(msg, status) {
                if (status = 200) {
                    Materialize.toast('Registration Successful Click on Login to Continue', 5000);
                } else {
                    Materialize.toast('Registration Failed please Try Again', 5000);
                }
            }

        });
    };
    var user_login = function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: url,
            data: $("#login").serialize(),
            success: function(response, status) {
                if (status = 200) {
                    localStorage.setItem("userdata", response);
                    var url = document.location.origin + "/html/dashboard.html";
                    window.location.replace(url);
                } else {
                    Materialize.toast('Login Failed please Try Again', 3000);
                }
            }
        });
    };

    return {
        login: function(){
            $('#login_btn').click(function(e){
            user_login(e);
        })},
        register: function(){
            $('#register_btn').click(function(e){
            user_register(e);
        })}
    };
}();
